#include <stdio.h>
#include <getopt.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb/stb_image_resize.h"

#define R i
#define G i + 1
#define B i + 2

void img_to_grayscale(unsigned char *, int, int, int);
void img_increase_contrast(unsigned char *, int, int, int, float);
void img_scale_down(unsigned char *, int *, int *, int, int);
char *img_to_ascii(unsigned char *, int, int, int);

int main(int argc, char ** argv) {
    int opt, scale = 1;
    float gain = 1.0;
    char *outfile = "out.txt", *infile = NULL;
    while ((opt = getopt (argc, argv, "F:O:G:S:")) != -1) {
        switch(opt) {
                case 'F':
                    infile = optarg;
                    break;
                case 'O':
                    outfile = optarg;
                    break;
                case 'G':
                    gain = atof(optarg);
                    break;
                case 'S':
                    scale = atoi(optarg);
                    break;
                default:
                    break;
        }
    }
    if (infile == NULL) {
        fprintf(stderr, "No input file specified.\n");
        return EXIT_FAILURE;
    }
    int w, h, n_channels;
    unsigned char *img = stbi_load(infile, &w, &h, &n_channels, 0);
    img_to_grayscale(img, w, h, n_channels);
    img_increase_contrast(img, w, h, n_channels, gain);
    img_scale_down(img, &w, &h, n_channels, scale);
    char *ascii = img_to_ascii(img, w, h, n_channels);
    stbi_image_free(img);
    FILE * file = fopen(outfile, "w");
    fprintf(file, "%s\n", ascii);
    fclose(file);
    free(ascii);
}

void img_to_grayscale(unsigned char *img, int width, int height, int num_channels) {
    for (int i = 0; i < width * height * num_channels; i += num_channels) {
        unsigned char new_color = 0.21 * img[R] + 0.72 * img[G] + 0.07 * img[B];
        img[R] = new_color;
        img[G] = new_color;
        img[B] = new_color;
    }
}

void img_increase_contrast(unsigned char *img, int width, int height, int num_channels, float gain) {
    for (int i = 0; i < width * height * num_channels; i += num_channels) {
        int val = (int)img[R];
        int new_val = gain * (val - 128) + 128;
        if (new_val > 255) {
            new_val = 255;
        } else if (new_val < 0) {
            new_val = 0;
        }
        img[R] = (unsigned char)new_val;
        img[G] = (unsigned char)new_val;
        img[B] = (unsigned char)new_val;
    }
}

void img_scale_down(unsigned char *img, int *width, int *height, int num_channels, int scale_factor) {
    stbir_resize_uint8(img, *width, *height, 0, img, *width / scale_factor, *height / (scale_factor * 2), 0, num_channels);
    *width  /= scale_factor;
    *height /= (scale_factor * 2);
}

char *img_to_ascii(unsigned char *img, int width, int height, int num_channels) {
    char *map = "$@B&%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/|()1{}[]?-_+~<>i!lI;:,\"^`'. ";
    char *ascii = malloc(((width + 1) * height) + 2);
    int c = 0;
    for (int i = 0; i < width * height * num_channels; i += num_channels) {
        int val = (int)img[R];
        if (c % (width + 1) == 0) {
            ascii[c++] = '\n';
        }
        ascii[c++] = map[val * 69 / 255];
    }
    ascii[c] = '\0';
    return ascii;
}