#### img-to-ascii

A simple program that converts images to ASCII text.

Build with `make img-to-ascii`

Run with `./img-to-ascii -F image -O [outfile] -G [gamma] -S [scale] `
